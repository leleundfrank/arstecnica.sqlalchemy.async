# -*- coding: utf-8 -*-
# :Project:   arstecnica.sqlalchemy.async
# :Created:   mer 09 set 2015 16:34:44 CEST
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2015 Lele Gaifax
#

all: help

include Makefile.release
